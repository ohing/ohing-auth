var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var session = require('express-session');
var MySqlStore = require('express-mysql-session')(session);
var options = {
  host: 'localhost',
  port: 3306,
  user: 'ipin',
  password: 'Ohing52!@#',
  database: 'ipin'
}
var sessionStore = new MySqlStore(options)

var devIndexRouter = require('./routes/dev-index');
var indexRouter = require('./routes/index');

var passDevIndexRouter = require('./routes/pass-dev-index');
var passIndexRouter = require('./routes/pass-index');

var naverDevIndexRouter = require('./routes/naver-dev-index');
var naverIndexRouter = require('./routes/naver-index');

process.env.TZ = 'Asia/Seoul'

var app = express();

app.use(session({
  secret: 'ohing52!!!ohing',
  resave: false,
  saveUninitialized:true,
  store: sessionStore
}))

const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({extended: true}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/dev/naver', naverDevIndexRouter)
app.use('/naver', naverIndexRouter)

app.use('/dev/pass', passDevIndexRouter)
app.use('/pass', passIndexRouter)

app.use('/dev', devIndexRouter)
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
