var express = require('express');
var router = express.Router();

const axios = require('axios')

const clientId = '949pjBB25w5ohgd1hNPd'
const clientSecret = 'rGJmRGTlvA'

const loginCallbackUrl = 'https://auth.ohing.net/dev/naver/result/login'
const loginUrl = `https://nid.naver.com/oauth2.0/authorize?response_type=code&client_id=${clientId}&redirect_uri=${loginCallbackUrl}&state=`
const apiUrl = `https://nid.naver.com/oauth2.0/token?grant_type=authorization_code&client_id=${clientId}&client_secret=${clientSecret}&code={{code}}&state={{state}}`
const naverAuthUrl = `https://nsign-gw.naver.com/sign/v2/sign/web`
const naverAuthResultUrl = `https://nsign-gw.naver.com/sign/v2/sign/web/result`
const authCallbackUrl = `https://auth.ohing.net/dev/naver/result/auth`

const callCenterNumber = '070-4010-5552';

const checkPointUrl = 'https://310sk0tdb4.execute-api.ap-northeast-2.amazonaws.com/auth/check'
const saveAuthInfoUrl = 'https://310sk0tdb4.execute-api.ap-northeast-2.amazonaws.com/auth'


router.get('/', (req, res, next) => {

  // 개발환경에서는 header의 imei 및 query의 imei를 인정
  let imei = req.header('imei') || req.query.imei
  console.log('naver', imei)

  if (imei == null || imei.length == 0) {
    
    res.render('error', {message: '헤더 정보가 올바르지 않습니다.'})
    return
  }

  req.session.imei = imei

  req.session.save(() => {

    let timestamp = (new Date()).getTime();

    let naverLoginUrl = `${loginUrl}${timestamp}`
    res.redirect(naverLoginUrl)
  })
});

router.get('/result/login', async (req, res, next) => {

  console.log(req.body)
  console.log(req.query)

  let code = req.query.code
  let state = req.query.state

  let authUrl = apiUrl.replace('{{code}}', code).replace('{{state}}', state)

  let params
  let result
  
  result = await axios.get(authUrl, {
    headers: {
      'X-Naver-Client-Id': clientId,
      'X-Naver-Client-Secret': clientSecret
    }
  })

  let token = result.data.access_token

  req.session.token = token

  req.session.save(async () => {

    let ipAddress = req.headers['x-forwarded-for'] || req.socket.remoteAddress 

    params = {
      templateCode: 'selfAuth_v1',
      callbackPageUrl: authCallbackUrl,
      deviceEnvs: {
        remoteAddress: ipAddress
      },
      contactInfo: {
        callCenterNo: callCenterNumber
      }
    }

    console.log('params', params)

    try {

      result = await axios.post(naverAuthUrl, params, {
        headers: {
          'X-Naver-Client-Id': clientId,
          'X-Naver-Client-Secret': clientSecret,
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        }
      })

      let resultData = result.data || {}

      console.log(resultData)

      if (resultData.rtnMsg != 'success') {
        res.send('잘못된 접근입니다.')
        return
      }

      let txId = resultData.txId
      let nextUrl = resultData.pollingPageUrl
      let id = resultData.id

      console.log(txId, nextUrl, id)

      if (txId == null || nextUrl == null || id == null) {
        res.send('잘못된 접근입니다.')
        return
      }

      params = {
        authType: 'NAVER',
        txId: txId
      }

      result = await axios.put(checkPointUrl, params, {
        headers: {
          'Content-Type': 'application/json',
          'imei': req.session.imei
        }
      })

      console.log(result.data)

      res.redirect(nextUrl)

    } catch (e) {
      console.error(e)
      res.render('fail')
    }
  })
})

router.get('/result/auth', async (req, res, next) => {

  let txId = req.query.txId

  try {

    let result

    let url = `${naverAuthResultUrl}?txId=${txId}`

    result = await axios.get(url, {
      headers: {
        'X-Naver-Client-Id': clientId,
        'X-Naver-Client-Secret': clientSecret,
        'Authorization': `Bearer ${req.session.token}`,
        'Content-Type': 'application/json'
      }
    })

    console.log(result.data)

    let resultData = result.data

    let profile = resultData.profile
    
    if (profile == null) {
      res.render('fail')
      return
    }

    if (profile.ci == null) {
      res.render('fail')
      return
    }

    let now = new Date()

    let birthYear = parseInt(profile.birthyear)
    let birthDates = profile.birthday.split('-')
    let birthMonth = parseInt(birthDates[0])
    let birthDay = parseInt(birthDates[1])
    let age = now.getFullYear() - birthYear
    if (birthMonth > (now.getMonth()+1) || birthDay > now.getDate()) {
      age -= 1
    }

    // 개발서버에서는 반영 안함
    // if (age < 10) {
    //   console.log('under10')
    //   res.render('naver_result', {message: '만 10세 미만은 가입하실 수 없습니다.', code: 'failUnder10'})
    //   return
    // } else if (age > 19) {
    //   console.log('over19')
    //   res.render('naver_result', {message: '만 20세 이상은 가입하실 수 없습니다.', code: 'failOver19'})
    //   return
    // }

    let params = {
      authType: 'NAVER',
      imei: req.session.imei,
      name: profile.name,
      birthday: `${profile.birthyear}${profile.birthday.replace('-','')}`,
      localCode: '01',
      phoneNumber: profile.mobile.replace(/-/gi, ''),
      genderCode: profile.gender,
      ci: profile.ci,
      di: profile.id,
      txId: txId
    }

    result = await axios.put(saveAuthInfoUrl, params)

    let authId = result.data.authId

    if (age < 14) {
      console.log('under14')
      res.render('naver_result', {message: '만 14세 미만은 보호자의 동의가 필요합니다.', code: 'needsParentsAuth'})
      return
    }

    if (authId == null) {
      console.log('duplicated')
      res.render('naver_result', {message: '이미 오잉에 가입하셨습니다. 로그인 또는 아이디/비밀번호 찾기를 이용해주세요.\n문의: service@ohing.net', code: 'failDuplicateJoin'})
      return
    }

    console.log('success!')

    res.render('naver_result', {message: '인증 성공.', authId: authId, code: 'successSelfAuth'})

  } catch (e) {
    console.error(e.response || e)
    res.render('fail')
  }
})

module.exports = router;
