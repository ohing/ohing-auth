var express = require('express');
var router = express.Router();
const childProcess = require("child_process")

const axios = require('axios')

const siteCode = 'FW13'
const password = 'Ohing52ohing!!'
const modulePath = '/home/bitnami/projects/ipin/bin/IPIN2Client'

const processUrl = 'https://auth.ohing.net/ipin/process'
const saveAuthEndpoint = 'https://c1sx40k2mj.execute-api.ap-northeast-2.amazonaws.com/auth'

/* GET home page. */

router.get('/test', function(req, res, next) {

  res.render('ipin_result', {message: '만 14세 미만은 보호자의 동의가 필요합니다.', code: 'needsParentsAuth'})
})

router.get('/', function(req, res, next) {

  // 테스트용은 parameter에 붙은 imei도 인정
  let imei = req.header('imei') || req.param('imei')
  console.log(imei)

  if (imei == null || imei.length == 0) {
    res.render('error', {message: '헤더 정보가 올바르지 않습니다.'})
    return
  }

  req.session.imei = imei

  req.session.save(() => {
    let timestamp = (new Date()).getTime();
    let requestCode = `${siteCode}_${timestamp}`

    let encryptedData = ''
    let returnMessage = ''

    let command = `${modulePath} REQ ${siteCode} ${password} ${requestCode} ${processUrl}`

    let child = childProcess.exec(command , {encoding: "euc-kr"});
    child.stdout.on("data", function(data) {
      encryptedData += data;
    });
    child.on("close", function() {
      console.log(encryptedData);
    
      if (encryptedData == '-9') {
        returnMessage = '입력값 오류 : 암호화 처리시, 필요한 파라미터 값을 확인해 주시기 바랍니다.';
      }
      res.render('ipin_main', {encryptedData: encryptedData, returnMessage: returnMessage});
    });
  })
});

router.get('/process', async function(req, res, next) {

  let encryptedData = req.param('enc_data');
  console.log(encryptedData)

  try {

    let result = await processAuthResult(encryptedData, req)
    res.render('ipin_result', result)

  } catch (e) {
    console.error(e)
    res.render('ipin_result', {message: '10대 인증에 실패했습니다. 정보 확인 후 다시 시도해주세요.\n문의: service.ohing.net', code: 'fail'})
  }
});

router.post('/process', async function(req, res, next) {

  console.log(req.body)
  let encryptedData = req.body.enc_data

  try {

    let result = await processAuthResult(encryptedData, req)
    res.render('ipin_result', result)

  } catch (e) {
    console.error(e)
    res.render('ipin_result', {message: '10대 인증에 실패했습니다. 정보 확인 후 다시 시도해주세요.\n문의: service.ohing.net', code: 'fail'})
  }
});

function processAuthResult(encryptedData, req) {

  return new Promise(async (resolve, reject) => {
    try {

      let imei = req.session.imei
      let decryptedObject = await decryptedData(encryptedData, imei)
  
      console.log(decryptedObject)
  
      let returnMessage = decryptedObject.returnMessage
  
      if (returnMessage.length == 0) {
  
        let result = decryptedObject.result
  
        let birthDate = result.birthday
        let birthYear = parseInt(birthDate.substring(0, 4))
        let birthMonth = parseInt(birthDate.substring(4, 6))
        let birthDay = parseInt(birthDate.substring(6, 8))
  
        let now = new Date()
  
        let age = now.getFullYear() - birthYear
        if (birthMonth > (now.getMonth()+1) || birthDay > now.getDate()) {
          age -= 1
        }
        
        if (age < 10) {
          resolve({message: '만 10세 미만은 가입하실 수 없습니다.', code: 'failUnder10'})
          return
        } else if (age > 19) {
          resolve({message: '만 20세 이상은 가입하실 수 없습니다.', code: 'failOver19'})
          return
        }
  
        let parameters = result
        parameters.authType = 'IPIN'
        let saveAuthResult = await axios.put(saveAuthEndpoint, parameters)
        console.log(saveAuthResult)
        let authId = saveAuthResult.data.authId
  
        if (age < 14) {
          resolve({message: '만 14세 미만은 보호자의 동의가 필요합니다.', code: 'needsParentsAuth'})
          return
        }
  
        if (authId == null) {
          resolve({message: '이미 오잉에 가입하셨습니다. 로그인 또는 아이디/비밀번호 찾기를 이용해주세요.\n문의: service@ohing.net', code: 'failDuplicateJoin'})
        } else {
          resolve({message: '인증 성공.', authId: authId, code: 'successSelfAuth'})
        }
      } else {
        resolve({message: returnMessage, code: 'fail'})
      }
  
    } catch (e) {
      reject(e)
    }
  })
}

function decryptedData(encryptedData, imei) {

  return new Promise((resolve, reject) => {

    let command = `${modulePath} RES ${siteCode} ${password} ${encryptedData}`

    let executor = childProcess.exec(command, {encoding: 'euc-kr'});
    let decrypted = ''
    executor.stdout.on('data', function(data) {
      decrypted += data
    })
    executor.on('close', function() {
      console.log('data', decrypted);
      let returnMessage = ''
  
      if (decrypted == '-9') {
        returnMessage = '입력값 오류. 암호화 처리시, 필요한 파라미터 값을 확인해 주시기 바랍니다.';
      }

      let result = {}

      let resultCode = decodeURIComponent(getValue(decrypted, "RESULT_CODE"));
      console.log('resultCode', resultCode)

      if (resultCode == 1) {

        if (imei == null) {
          returnMessage = '잘못된 요청입니다.'
        }

        result.imei = imei
        result.virtualNumber = decodeURIComponent(getValue(decrypted, "VNUMBER"));      // 가상주민번호 (13Byte, 영숫자 조합)
        result.name = decodeURIComponent(getValue(decrypted, "UTF8_NAME"));    // 이름 (EUC-KR)
        result.ageCode = decodeURIComponent(getValue(decrypted, "AGECODE"));      // 연령대 코드 (0~7: 가이드 참조)
        result.genderCode = decodeURIComponent(getValue(decrypted, "GENDERCODE"));   // 성별 코드 (0:여성, 1: 남성)
        result.birthday = decodeURIComponent(getValue(decrypted, "BIRTHDATE"));    // 생년월일 (YYYYMMDD)
        result.nationalInfo = decodeURIComponent(getValue(decrypted, "NATIONALIFO"));  // 내/외국인 정보 (0:내국인, 1:외국인)
        result.strRequestNumber = decodeURIComponent(getValue(decrypted, "CPREQUESTNO"));  // CP요청번호 (main에서 생성되어 인증 시 전달된 값)
        result.di = getValue(decrypted, "DUPINFO");                          // 중복가입 확인값(DI, 64byte)
        result.ci = getValue(decrypted , "COINFO1");                         // 연계정보 확인값(CI, 88byte)
        result.ciUpdate = getValue(decrypted , "CIUPDATE");                        // CI 갱신정보 (1~: 가이드 참조)

      } else {
        returnMessage = '인증에 실패하였습니다.\n문의: service.ohing.net'
      }

      resolve({result: result, returnMessage: returnMessage})
    })
  })
}

function getValue(plaindata, key) {
  var arrData = plaindata.split(":");
  var value = "";

  for(i in arrData){
    var item = arrData[i];
    if(item.indexOf(key) == 0)
    {
      var valLen = parseInt(item.replace(key, ""));
      arrData[i++];
      value = arrData[i].substr(0 ,valLen);
      break;
    }
  }
  return value;
}

module.exports = router;
