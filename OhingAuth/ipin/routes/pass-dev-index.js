var express = require('express');
var router = express.Router();
const childProcess = require("child_process")

const axios = require('axios')

const siteCode = 'BW124'
const password = 'ZQtiAnPSYvqJ'
const modulePath = '/home/bitnami/projects/ipin/bin/CPClient_64bit'

const successUrl = 'https://auth.ohing.net/dev/pass/success'
const errorUrl = 'https://auth.ohing.net/dev/pass/error'
const saveAuthEndpoint = 'https://310sk0tdb4.execute-api.ap-northeast-2.amazonaws.com/auth'
const saveParentsAuthEndpoint = 'https://310sk0tdb4.execute-api.ap-northeast-2.amazonaws.com/auth/parents'

/* GET home page. */
router.get('/', function(req, res, next) {

  // 개발환경에서는 header의 imei 및 query의 imei를 인정
  let imei = req.header('imei') || req.query.imei
  console.log(imei)

  if (imei == null || imei.length == 0) {
    
    res.render('error', {message: '헤더 정보가 올바르지 않습니다.'})
    return
  }

  req.session.imei = imei
  req.session.is_parents = '0'

  req.session.save(async () => {
    let date = new Date()

    let requestCode = `siteCode_${date.getTime()}_${imei}`
    let plainData = `7:REQ_SEQ${requestCode.length}:${requestCode}8:SITECODE${siteCode.length}:${siteCode}9:AUTH_TYPE0:7:RTN_URL${successUrl.length}:${successUrl}7:ERR_URL${errorUrl.length}:${errorUrl}9:CUSTOMIZE6:Mobile`

    try {

      let encrypted = await encryptedData(plainData)
      let returnMessage = encrypted.returnMessage

      if (returnMessage.length == 0) {
        res.render('pass_main', {encryptedData: encrypted.encryptedData, returnMessage: returnMessage});
      } else {
        res.render('pass_result', {message: returnMessage, code: 'fail'})
      }

    } catch (e) {
      console.log(e)
      res.render('pass_result', {message: '10대 인증에 실패했습니다. 정보 확인 후 다시 시도해주세요.\n문의: service.ohing.net', code: 'fail'})
    }
  })
});

router.get('/parents', async function(req, res, next) {

  // 개발환경에서는 header의 imei 및 query의 imei를 인정
  let imei = req.header('imei') || req.query.imei
  console.log(imei)

  if (imei == null || imei.length == 0) {
    
    res.render('error', {message: '헤더 정보가 올바르지 않습니다.'})
    return
  }

  req.session.imei = imei
  req.session.is_parents = '1'

  req.session.save(async () => {
    let date = new Date()

    let requestCode = `siteCode_${date.getTime()}_${imei}`
    let plainData = `7:REQ_SEQ${requestCode.length}:${requestCode}8:SITECODE${siteCode.length}:${siteCode}9:AUTH_TYPE0:7:RTN_URL${successUrl.length}:${successUrl}7:ERR_URL${errorUrl.length}:${errorUrl}9:CUSTOMIZE6:Mobile`

    try {

      let encrypted = await encryptedData(plainData)
      let returnMessage = encrypted.returnMessage

      if (returnMessage.length == 0) {
        res.render('pass_main', {encryptedData: encrypted.encryptedData, returnMessage: returnMessage});
      } else {
        res.render('pass_result', {message: returnMessage, code: 'fail'})
      }

    } catch (e) {
      console.log(e)
      res.render('pass_result', {message: '부모 인증에 실패했습니다. 정보 확인 후 다시 시도해주세요.\n문의: service.ohing.net', code: 'fail'})
    }
  })
})



router.get('/success', async function(req, res, next) {

  console.log('get success!')

  let encryptedData = req.param['EncodeData'];
  console.log(encryptedData)

  try {

    let result = await processAuthResult(encryptedData, req)
    res.render('pass_result', result)

  } catch (e) {
    console.log(e)
    res.render('pass_result', {message: '10대 인증에 실패했습니다. 정보 확인 후 다시 시도해주세요.\n문의: service.ohing.net', code: 'fail'})
  }
});

router.post('/success', async function(req, res, next) {
  
  console.log('post success!')

  let encryptedData = req.body.EncodeData;
  console.log(encryptedData)

  try {

    let result = await processAuthResult(encryptedData, req)
    res.render('pass_result', result)

  } catch (e) {
    console.log(e)
    res.render('pass_result', {message: '10대 인증에 실패했습니다. 정보 확인 후 다시 시도해주세요.\n문의: service.ohing.net', code: 'fail'})
  }
});

router.get('/error', async function(req, res, next) {

  console.log(req.body)
  let encryptedData = req.param['EncodeData']

  try {

    let result = await processAuthResult(encryptedData, req)
    res.render('pass_result', result)

  } catch (e) {
    console.error(e)
    res.render('pass_result', {message: '10대 인증에 실패했습니다. 정보 확인 후 다시 시도해주세요.\n문의: service.ohing.net', code: 'fail'})
  }
});

router.post('/error', async function(req, res, next) {

  console.log(req.body)
  let isParents = req.session.is_parents == '1'

  let encryptedData = req.body.EncodeData

  try {

    let result = await processAuthResult(encryptedData, req)
    res.render('pass_result', result)

  } catch (e) {
    console.error(e)
    res.render('pass_result', {message: `${isParents ? '부모' : '10대'} 인증에 실패했습니다. 정보 확인 후 다시 시도해주세요.\n문의: service.ohing.net`, code: 'fail'})
  }
});

function processAuthResult(encryptedData, req) {

  return new Promise(async (resolve, reject) => {
    try {

      let imei = req.session.imei
      let isParents = req.session.is_parents == '1'
      
      let decryptedObject = await decryptedData(encryptedData, imei)
      
      let returnMessage = decryptedObject.returnMessage
  
      if (returnMessage.length == 0) {
  
        let result = decryptedObject.result

        if (!isParents) {
          let birthDate = result.birthday
          let birthYear = parseInt(birthDate.substring(0, 4))
          let birthMonth = parseInt(birthDate.substring(4, 6))
          let birthDay = parseInt(birthDate.substring(6, 8))
    
          let now = new Date()
    
          let age = now.getFullYear() - birthYear
          if (birthMonth > (now.getMonth()+1) || birthDay > now.getDate()) {
            age -= 1
          }
    
          // 개발서버에서는 반영 안함
          // if (age < 10) {
          //   resolve({message: '만 10세 미만은 가입하실 수 없습니다.', code: 'failUnder10'})
          //   return
          // } else if (age > 19) {
          //   resolve({message: '만 20세 이상은 가입하실 수 없습니다.', code: 'failOver19'})
          //   return
          // }

          let parameters = result
          let saveAuthResult = await axios.put(saveAuthEndpoint, parameters)
          console.log(saveAuthResult)
          let authId = saveAuthResult.data.authId
    
          if (age < 14) {
            resolve({message: '만 14세 미만은 보호자의 동의가 필요합니다.', code: 'needsParentsAuth'})
            return
          }
    
          if (authId == null) {
            resolve({message: '이미 오잉에 가입하셨습니다. 로그인 또는 아이디/비밀번호 찾기를 이용해주세요.\n문의: service@ohing.net', code: 'failDuplicateJoin'})
          } else {
            resolve({message: '인증 성공.', authId: authId, code: 'successSelfAuth'})
          }

        } else {

          let parameters = result
          let saveAuthResult = await axios.put(saveParentsAuthEndpoint, parameters)
          console.log(saveAuthResult)
          let authId = saveAuthResult.data.authId

          if (authId == null) {
            resolve({message: '보호자 동의에 실패했습니다. 정보 확인 후 다시 시도해주세요.\n문의: service@ohing.net', code: 'fail'})
          } else {
            resolve({message: '인증 성공.', authId: authId, code: 'successParentsAuth'})
          }
        }

      } else {
        resolve({message: returnMessage, code: 'fail'})
      }
  
    } catch (e) {
      console.log('error', e)
      reject(e)
    }
  })
}

function encryptedData(plainData) {

  return new Promise((resolve, reject) => {

    let encryptedData = ''
    let returnMessage = ''

    let command = `${modulePath} ENC ${siteCode} ${password} ${plainData}`

    let child = childProcess.exec(command, {encoding: "euc-kr"});
    child.stdout.on("data", function(data) {
      encryptedData += data;
    });
    child.on("close", function() { 
      console.log(encryptedData);

      switch (encryptedData) {
        case '-1': {
          returnMessage = '암/복호화 시스템 오류입니다.'
          break
        }
        case '-2': {
          returnMessage = '암호화 처리 오류입니다.'
          break
        }
        case '-3': {
          returnMessage = '암호화 데이터 오류 입니다.'
          break
        }
        case '-9': {
          returnMessage = '입력값 오류 : 암호화 처리시, 필요한 파라미터 값을 확인해 주시기 바랍니다.';
          break
        }
        default: {
          returnMessage = ''
          break
        }
      }

      console.log(returnMessage)
      resolve({returnMessage: returnMessage, encryptedData: encryptedData})
    });
  })
}

function decryptedData(encryptedData, imei) {

  return new Promise((resolve, reject) => {

    let returnMessage = ''

    if (imei == null) {
      returnMessage = '잘못된 요청입니다.'
      resolve({result: {}, returnMessage: returnMessage})
      return
    }

    if( /^0-9a-zA-Z+\/=/.test(encryptedData) == true || encryptedData.length == ''){
      returnMessage = '10대 인증에 실패했습니다. 정보 확인 후 다시 시도해주세요.\n문의: service.ohing.net'
      resolve({result: {}, returnMessage: returnMessage})
      return
    }

    let command = `${modulePath} DEC ${siteCode} ${password} ${encryptedData}`

    console.log(command)

    let decryptedData = ''

    let child = childProcess.exec(command, {encoding: "euc-kr"});
    child.stdout.on("data", function(data) {
      decryptedData += data;
    });
    child.on("close", function() {
      console.log(decryptedData)

      let result = {}

      switch (decryptedData) {
        case '-1': {
          returnMessage = '암/복호화 시스템 오류입니다.'
          break
        }
        case '-4': {
          returnMessage = '복호화 처리 오류입니다.'
          break
        }
        case '-5': {
          returnMessage = 'HASH값 불일치'
          break
        }
        case '-6': {
          returnMessage = '복호화 데이터 오류'
          break
        }
        case '-9': {
          returnMessage = '입력값 오류';
          break
        }
        case '-12': {
          returnMessage = '사이트 비밀번호 오류';
          break
        }
        default: {
          returnMessage = ''

          let requestnumber = decodeURIComponent(getValue(decryptedData , "REQ_SEQ"));     //CP요청 번호 , main에서 생성한 값을 되돌려준다. 세션등에서 비교 가능

          let requestedImei = requestnumber.split('_')[2]

          if (requestedImei != imei) {

          }

          let responsenumber = decodeURIComponent(getValue(decryptedData , "RES_SEQ"));    //고유 번호 , 나이스에서 생성한 값을 되돌려준다.
          let authtype = decodeURIComponent(getValue(decryptedData , "AUTH_TYPE"));        //인증수단
          let name = decodeURIComponent(getValue(decryptedData , "UTF8_NAME"));            //이름
          let birthdate = decodeURIComponent(getValue(decryptedData , "BIRTHDATE"));       //생년월일(YYYYMMDD)
          let gender = decodeURIComponent(getValue(decryptedData , "GENDER"));             //성별
          let nationalinfo = decodeURIComponent(getValue(decryptedData , "NATIONALINFO")); //내.외국인정보
          let dupinfo = decodeURIComponent(getValue(decryptedData , "DI"));                //중복가입값(64byte)
          let conninfo = decodeURIComponent(getValue(decryptedData , "CI"));               //연계정보 확인값(88byte)
          let mobileno = decodeURIComponent(getValue(decryptedData , "MOBILE_NO"));        //휴대폰번호(계약된 경우)
          let mobileco = decodeURIComponent(getValue(decryptedData , "MOBILE_CO"));        //통신사(계약된 경우)

          result = {
            authType: mobileco,
            imei: imei,
            name: name,
            birthday: birthdate,
            localCode: nationalinfo,
            phoneNumber: mobileno,
            genderCode: (gender == '1' || gender == 'M') ? 'M' : 'F',
            di: dupinfo,
            ci: conninfo,
          }

          break
        }
      }

      console.log(returnMessage, result)

      resolve({result: result, returnMessage: returnMessage})
    })
  })
}

function getValue(plaindata, key) {
  var arrData = plaindata.split(":");
  var value = "";

  for(i in arrData){
    var item = arrData[i];
    if(item.indexOf(key) == 0)
    {
      var valLen = parseInt(item.replace(key, ""));
      arrData[i++];
      value = arrData[i].substr(0 ,valLen);
      break;
    }
  }
  return value;
}

module.exports = router;
